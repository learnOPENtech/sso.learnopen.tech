FROM quay.io/keycloak/keycloak:26.1
RUN /opt/keycloak/bin/kc.sh build --db=postgres
ADD themes /opt/keycloak/themes
